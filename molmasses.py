# todo поддержка нескольких языков
# todo сделать модулем
ion1, zar1, mass1 = input('Введите информацию о первом ионе в формате "H 1 1"').split()
# TODO доставать информацию об ионах из файла ions.txt
ion2, zar2, mass2 = input('Введите информацию о втором ионе в формате "SO4 -2 96"').split()


def mmcount(z1, m1, z2, m2):
    k1 = 1
    k2 = 1
    if abs(z1 % z2) == 0:
        k2 = z1 // abs(z2)
    elif abs(z2 % z1) == 0:
        k1 = abs(z2) // z1
    elif abs(z1 / z2) == 2 / 3:
        k1 = 3
        k2 = 2
    elif abs(z2 / z1) == 2 / 3:
        k2 = 3
        k1 = 2
    return k1 * m1 + k2 * m2


print(mmcount(int(zar1), int(mass1), int(zar2), int(mass2)))
